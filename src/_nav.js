export default {
  items: [
    {
      name: 'Dash-on',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Tools',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Materials',
      url: '/inventory/materials',
      icon: 'fa fa-th-large'
    },
    {
      name: 'Products',
      url: '/inventory/products',
      icon: 'fa fa-dropbox'
    },
    {
      name: 'Sales',
      url: '/inventory/sales',
      icon: 'fa fa-money'
    }
  ]
}
