class SaleAPI {
  static createS = '/api/sales/create'
  static findS = '/api/sales/find'
  static getAllS = '/api/sales/get'
  static getCreated = '/api/sales/created/'
  static findByClient = '/api/sales/find-by/client'
}

export default SaleAPI
