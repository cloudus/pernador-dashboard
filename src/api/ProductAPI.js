class ProductAPI {
  static createP = '/api/products/create'
  static addToInventory = '/api/products/add'
  static findP = '/api/products/find'
  static editP = '/api/products/edit'
  static trashP = '/api/products/trash'
  static getAllProds = '/api/products/get'
  static getAddedProducts = '/api/products/get/added/'
  static getCreatedProducts = '/api/products/get/created/'
}

export default ProductAPI
