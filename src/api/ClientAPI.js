class ClientAPI {
  static createC = '/api/clients/create'
  static findC = '/api/clients/find'
  static getCreated = '/api/clients/created/'
}

export default ClientAPI
