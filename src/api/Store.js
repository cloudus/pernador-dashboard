import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import axios from 'axios'

Vue.use(Vuex)

const Store = new Vuex.Store({
  plugins: [createPersistedState()],

  state: {
    tokens: {
      access_token: null,
      expires_in: null,
      refresh_token: null,
      token_type: null
    },
    currentUser: {
      name: null,
      email: null,
      id: null,
      lvl: null
    },
    axiosCalls: {
      login: false,
      refreshingToken: false,
      updatingData: false
    }
  },
  getters: {
    tokens: state => {
      return state.tokens
    },
    currentUser (state) {
      return state.currentUser
    },
    axiosCalls (state) {
      return state.axiosCalls
    }
  },
  actions: {
    login: function (context, user) {
      return new Promise((resolve, reject) => {
        let loginData = {
          username: user.username,
          password: user.password
        }

        if (!this.getters.axiosCalls.login) {
          let flag = {
            nameFlag: 'login',
            valueFlag: true
          }
          context.commit('asyncFlagging', flag)

          axios.post(API.login, loginData)
            .then(response => {
              let responseData = response.data
              let now = Math.floor(Date.now() / 1000)

              responseData.auth.expires_in = responseData.auth.expires_in + now

              context.commit('updateTokens', responseData.auth)
              context.commit('updateUserData', responseData.userData)

              flag.valueFlag = false
              context.commit('asyncFlagging', flag)

              resolve(response)
              window.router.push('/')
            })
            .catch(error => {
              flag.valueFlag = false
              context.commit('asyncFlagging', flag)
              console.log('Error on login', error.response.data)
              reject(error.response.data)
            })
        }
      })
    },
    logout (context) {
      return new Promise((resolve, reject) => {
        this.dispatch('resetData')
        window.router.push('/login')
      })
    },
    checkTokenValidity (context) {
      return new Promise((resolve, reject) => {
        if (!this.getters.tokens.refresh_token) {
          this.dispatch('logout')
            .then(response => {
              window.router.push('/login')
            })
            .catch(error => {
              console.log('checkTokenValidity logout error', error)
            })
        } else if (this.getters.tokens.expires_in * 1000 < Date.now() || !this.getters.tokens.access_token) {
          let thisObj = this
          this.dispatch('refreshToken')
            .then(response => {
              // Dispatch User info update after every refresh
              thisObj.dispatch('updateUserData')
                .then(response => {
                })
                .catch(error => {
                  reject(error)
                })

              resolve('in-session')
            })
            .catch(error => {
              console.log('Response refresh error', error)
              reject(error)
              window.router.push('/login')
            })
        } else {
          this.dispatch('updateUserData')
            .then(response => {
            })
            .catch(error => {
              reject(error)
            })
          resolve('in-session')
        }
      })
    },
    refreshToken: function (context) {
      return new Promise((resolve, reject) => {
        let loginData = {
          refresh_token: this.getters.tokens.refresh_token
        }

        if (!this.getters.axiosCalls.refreshingToken) {
          let flag = {
            nameFlag: 'refreshingToken',
            valueFlag: true
          }
          context.commit('asyncFlagging', flag)

          axios.post(API.refresher, loginData)
            .then(response => {
              let responseData = response.data
              let now = Math.floor(Date.now() / 1000)

              responseData.expires_in = responseData.expires_in + now

              context.commit('updateTokens', responseData)
              flag.valueFlag = false
              context.commit('asyncFlagging', flag)
              resolve(response)
            })
            .catch(error => {
              console.log('Error on login store', error)
              flag.valueFlag = false
              context.commit('asyncFlagging', flag)
              reject(error)
            })
        }
      })
    },
    updateUserData (context) {
      return new Promise((resolve, reject) => {
        let postData = {
          method: 'GET',
          url: API.userDetails,
          headers: {
            'Access-Control-Allow-Headers': 'Authorization',
            'Access-Control-Allow-Origin': '*',
            'Authorization': Store.getters.tokens.token_type + ' ' + Store.getters.tokens.access_token
          }
        }
        if (!this.getters.axiosCalls.updatingData) {
          let flag = {
            nameFlag: 'updatingData',
            valueFlag: true
          }
          context.commit('asyncFlagging', flag)

          API.makeAdminApi(API.getUserData, postData, 'GET')
            .then(response => {
              context.commit('updateUserData', response.success)
              flag.valueFlag = false
              context.commit('asyncFlagging', flag)
              resolve(response)
            })
            .catch(error => {
              flag.valueFlag = false
              context.commit('asyncFlagging', flag)
              reject(error)
            })
        }
      })
    },
    resetData (context) {
      context.commit('clearUserData')
      context.commit('clearTokens')
      context.commit('clearFlags')
    }
  },
  mutations: {
    clearFlags (state) {
      state.axiosCalls = {
        login: false,
        refreshingToken: false,
        updatingData: false
      }
    },
    updateTokens (state, tokens) {
      state.tokens = tokens
    },
    updateUserData (state, userData) {
      state.currentUser = userData
      console.log('Updated User Data')
    },
    clearUserData (state) {
      state.currentUser = {
        name: null,
        email: null,
        userId: null,
        lvl: null
      }
    },
    clearTokens (state) {
      state.tokens = {
        access_token: null,
        expires_in: null,
        refresh_token: null,
        token_type: null
      }
    },
    asyncFlagging (state, flag) {
      state.axiosCalls[flag.nameFlag] = flag.valueFlag
    }
  }
})

export default Store
