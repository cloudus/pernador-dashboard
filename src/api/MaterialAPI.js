class MaterialAPI {
  static createMaterial = '/api/materials/create'
  static addMaterialToInventory = '/api/materials/add'
  static findMaterial = '/api/materials/find'
  static editM = '/api/materials/edit'
  static trashM = '/api/materials/trash'
  static getAllMats = '/api/materials/get'
  static getAddedMaterials = '/api/materials/get/added/'
  static getCreatedMaterials = '/api/materials/get/created/'
}

export default MaterialAPI
