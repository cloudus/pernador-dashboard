class API {
  static register = '/api/auth/registerAdmin'
  static login = '/api/auth/login'
  static refresher = '/api/auth/refresh'

  static generateTBO = '/api/admin/create-user'
  static getUserData = '/api/details'

  static makeAdminApi (url, postInfo, method = 'POST', headers = {}) {
    return new Promise((resolve, reject) => {
      let postData = {
        method: method,
        url: url,
        data: postInfo,
        headers: {
          'Access-Control-Allow-Headers': 'Authorization',
          'Access-Control-Allow-Origin': '*',
          'Authorization': Store.getters.tokens.token_type + ' ' + Store.getters.tokens.access_token
        }
      }
      postData.headers = Object.assign(postData.headers, headers)
      console.log(postData)
      axios(postData)
        .then(response => {
          let responseData = response.data
          console.log(response)
          resolve(responseData)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  static makeAsyncCall (url, postInfo, method = 'POST', headers = {}) {
    return new Promise((resolve, reject) => {
      Store.dispatch('checkTokenValidity')
        .then(response => {
          let postData = {
            method: method,
            url: url,
            data: postInfo,
            requestId: 'request_id',
            headers: {
              'Access-Control-Allow-Headers': 'Authorization',
              'Access-Control-Allow-Methods': '*',
              'Access-Control-Allow-Origin': '*',
              'Authorization': Store.getters.tokens.token_type + ' ' + Store.getters.tokens.access_token
            }
          }
          postData.headers = Object.assign(postData.headers, headers)
          console.log(postData)
          axios(postData)
            .then(response => {
              let responseData = response.data
              console.log(response)
              resolve(responseData)
            })
            .catch(error => {
              console.log(error)
              reject(error)
            })
            .catch(err => {
              reject(err)
            })
        })
        .catch(err => {
          reject(err)
        })
    })
  }
}

export default API
